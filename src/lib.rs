// exercise from https://rustyyato.github.io/type/system,type/families/2021/02/15/Type-Families-1.html

use std::marker::PhantomData;

// `Option` has the kind `Type -> Type`,
// we'll represent it with `OptionFamily`
struct OptionFamily;

// `Result` has the kind `Type -> Type -> Type`,
// so we fill in one of the types with a concrete one
struct ResultFamily<E>(PhantomData<E>);

struct VecFamily;

// This trait represents the `kind` `Type -> Type`
pub trait OneTypeParam<A> {
    // This represents the output of the function `Type -> Type`
    // for a specific argument `A`.
    type This;
}

impl<A> OneTypeParam<A> for OptionFamily {
    // `OptionFamily` represents `Type -> Type`,
    // so filling in the first argument means
    // `Option<A>`
    type This = Option<A>;
}

impl<A, E> OneTypeParam<A> for ResultFamily<E> {
    // note how all results in this family have `E` as the error type
    // This is similar to how currying works in functional languages
    type This = Result<A, E>;
}

impl<A> OneTypeParam<A> for VecFamily {
    type This = Vec<A>;
}

// Option<A> == This<OptionFamily, A>
pub type This<T, A> = <T as OneTypeParam<A>>::This;

trait Functor<A, B>: OneTypeParam<A> + OneTypeParam<B> {
    fn map<F>(self, this: This<Self, A>, f: F) -> This<Self, B>
    where
        F: Fn(A) -> B + Copy;
}

impl<A, B> Functor<A, B> for OptionFamily {
    fn map<F>(self, this: This<Self, A>, f: F) -> This<Self, B>
    where
        F: Fn(A) -> B + Copy {
        // I'm not cheating!
        this.map(f)
    }
}

impl<A, B, E> Functor<A, B> for ResultFamily<E> {
    fn map<F>(self, this: This<Self, A>, f: F) -> This<Self, B>
    where
        F: Fn(A) -> B + Copy {
        this.map(f)
    }
}

impl<A, B> Functor<A, B> for VecFamily {
    fn map<F>(self, this: This<Self, A>, f: F) -> This<Self, B>
    where
        F: Fn(A) -> B + Copy {
        this.into_iter().map(f).collect::<Vec<_>>()
    }
}

#[test]
fn option_functor_some() {
    let x: <OptionFamily as OneTypeParam<u32>>::This = Some(1);
    let y: Option<i32> = Some(-1);
    assert_eq!(OptionFamily.map(x, |a| (a as i32) - 2), y);
}

#[test]
fn option_functor_none() {
    let x: <OptionFamily as OneTypeParam<u32>>::This = None;
    let y: Option<i32> = None;
    assert_eq!(OptionFamily.map(x, |a| (a as i32) - 2), y);
}

#[test]
fn result_functor_ok() {
    let x: <ResultFamily<bool> as OneTypeParam<u32>>::This = Ok(1);
    let y: Result<i32, bool> = Ok(-1);
    assert_eq!(ResultFamily::<bool>(PhantomData::<bool>).map(x, |a| (a as i32) - 2), y);
}

#[test]
fn result_functor_err() {
    let x: <ResultFamily<bool> as OneTypeParam<u32>>::This = Err(false);
    let y: Result<i32, bool> = Err(false);
    assert_eq!(ResultFamily::<bool>(PhantomData::<bool>).map(x, |a| (a as i32) - 2), y);
}

#[test]
fn vector_functor() {
    let x: <VecFamily as OneTypeParam<u32>>::This = vec![1, 2, 3];
    let y: Vec<i32> = vec![-3, -2, -1];
    assert_eq!(VecFamily.map(x, |a| (a as i32) - 4), y);
}

trait Monad<A, B>: Functor<A, B> {
    fn bind<F>(self, a: This<Self, A>, f: F) -> This<Self, B>
    where
        F: Fn(A) -> This<Self, B> + Copy;
}

impl<A, B> Monad<A, B> for OptionFamily {
    fn bind<F>(self, this: This<Self, A>, f: F) -> This<Self, B>
    where
        F: Fn(A) -> This<Self, B> + Copy {
        // It fits 😉
        this.and_then(f)
    }
}

impl<A, B, E> Monad<A, B> for ResultFamily<E> {
    fn bind<F>(self, this: This<Self, A>, f: F) -> This<Self, B>
    where
        F: Fn(A) -> This<Self, B> + Copy {
        this.and_then(f)
    }
}

impl<A, B> Monad<A, B> for VecFamily {
    fn bind<F>(self, this: This<Self, A>, f: F) -> This<Self, B>
    where
        F: Fn(A) -> This<Self, B> + Copy {
        this.into_iter().map(f).flatten().collect()
    }
}

#[test]
fn option_monad_some() {
    let x: <OptionFamily as OneTypeParam<u32>>::This = Some(1);
    let y: Option<i32> = Some(-1);
    assert_eq!(OptionFamily.bind(x, |a| Some((a as i32) - 2)), y);
}

#[test]
fn option_monad_none() {
    let x: <OptionFamily as OneTypeParam<u32>>::This = None;
    let y: Option<i32> = None;
    assert_eq!(OptionFamily.bind(x, |a| Some((a as i32) - 2)), y);
}

#[test]
fn result_monad_ok() {
    let x: <ResultFamily<bool> as OneTypeParam<u32>>::This = Ok(1);
    let y: Result<i32, bool> = Ok(-1);
    assert_eq!(ResultFamily::<bool>(PhantomData::<bool>).bind(x, |a| Ok((a as i32) - 2)), y);
}

#[test]
fn result_monad_err() {
    let x: <ResultFamily<bool> as OneTypeParam<u32>>::This = Err(false);
    let y: Result<i32, bool> = Err(false);
    assert_eq!(ResultFamily::<bool>(PhantomData::<bool>).bind(x, |a| Ok((a as i32) - 2)), y);
}

#[test]
fn vector_monad() {
    let x: <VecFamily as OneTypeParam<u32>>::This = vec![1, 2, 3];
    let y: Vec<i32> = vec![-3, -2, -1];
    assert_eq!(VecFamily.bind(x, |a| vec![(a as i32) - 4]), y);
}
